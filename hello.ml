(*
 * CGI / back-end
 *)

(* The function is missing in OCaml 4.03 and older: implementing...
let rec split_on_char_ c s = let open String in
    try let i = index s c in let i' = i + 1 in
      (sub s 0 i) :: (split_on_char_ c (sub s i' ((length s) - i')))
    with Not_found -> [s] *)

(* Generating the XML prolog and the XHTML container *)

let xml_control name aa = let open String in
  let base = Xml.to_string (Xml.Element (name, aa, [])) in
  concat (sub base 1 (length base - 3)) ["<?"; "?>"]  (* "<", "/>" *)

(* The first diagnostic message: arguments and environment variables *)

let arguments = let xml_list_entry a = Xml.Element ("argument", [],
                                                   [Xml.PCData a]) in
  Xml.Element ("arguments", [],
               (List.map xml_list_entry (Array.to_list Sys.argv)))

let slots =
[ "AUTH_TYPE"
; "CONTENT_LENGTH"
; "CONTENT_TYPE"
; "GATEWAY_INTERFACE"
(* ; "HTTP_*" *)
; "PATH_INFO"
; "PATH_TRANSLATED"
; "QUERY_STRING"
; "REMOTE_ADDR"
; "REMOTE_HOST"
; "REMOTE_IDENT"
; "REMOTE_USER"
; "REQUEST_METHOD"
; "SCRIPT_NAME"
; "SERVER_NAME"
; "SERVER_PORT"
; "SERVER_PROTOCOL"
; "SERVER_SOFTWARE"
]
let environment =
  let xml_list_entry slot value = Xml.Element ("slot", [],
    [Xml.PCData (String.concat " = " [slot; value])]) in
  let xml_entry_of rest slot =
    try (xml_list_entry slot (Sys.getenv slot)) :: rest
    with Not_found -> rest in
  Xml.Element ("environment", [],
               (List.fold_left xml_entry_of [] slots))

(* The second diagnostic message: the form data *)

let pairs = Cgi.parse_args ()
let form_fields =
  let xml_list_entry (key, value) = Xml.Element ("field", [],
    [Xml.PCData (String.concat " = " [key; value])]) in
  Xml.Element ("fields", [], (List.map xml_list_entry pairs))
let greeting =
  let greet_as = try let s = List.assoc "user-name" pairs in
                     String.concat "" ["Hello, "; s; "!"]
                 with Not_found -> "You haven't introduced yourself." in
  Xml.Element ("greeting", [], [Xml.PCData greet_as])
;;
print_endline (xml_control "xml" ["version", "1.0";
                                  "encoding", "UTF-8"]);  (* prolog *)
print_endline (xml_control "xml-stylesheet" ["href", "/hello.xsl";
                                             "type", "text/xsl"]);
print_endline "<!DOCTYPE hello SYSTEM \"/hello.dtd\">";
print_string (Xml.to_string_fmt (Xml.Element
  ("hello", [], [arguments; environment; form_fields; greeting])))
;;
