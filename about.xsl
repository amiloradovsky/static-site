<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:me="https://interpretmath.space"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/me:about">
    <html lang="{@lang}" xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <link rel="stylesheet" type="text/css" href="about.css" />
        <meta http-equiv="Content-Type"
              content="text/xhtml+xml; charset=UTF-8"/>
        <title> About </title>
      </head>
      <body>

        <header>
          <p> <xsl:value-of select="me:introduction"/> </p>
        </header>

        <div-links>
          <div>
            <ul>
              <xsl:for-each select="me:resumes/me:resume">
                <li>
                  <a href="{@file}"> <xsl:value-of select="."/> </a>
                </li>
              </xsl:for-each>
            </ul>
          </div>
          <div>
            <ul>
              <xsl:for-each select="me:microblogs/me:account">
                <li>
                  <a rel="me" href="https://{@host}/@{@user}">
                    <xsl:value-of select="."/>
                  </a>
                </li>
              </xsl:for-each>
            </ul>
          </div>
          <div>
            <ul>
              <xsl:for-each select="me:messengers/me:XMPP">
                <a href="xmpp:{@user}@{@host}">
                  <xsl:value-of select="."/>
                </a>
              </xsl:for-each>
            </ul>
          </div>
          <div>
            <ul>
              <xsl:for-each select="me:gpg-keys/me:key">
                <a href="{@file}">
                  <xsl:value-of select="."/>
                </a>
              </xsl:for-each>
            </ul>
          </div>
        </div-links>

      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
